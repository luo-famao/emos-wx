import App from './App'
// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	...App
})
app.$mount()
// #endif
// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif
//移动端发出请求，填好url地址，在main.js中集中管理全局变量
// ​5-6 定义全局路径和封装Ajax（移动端）
//buseUrl是项目的本地地址。
let baseUrl = "http://192.168.159.1:8080/emos-wx-api"
//注册，向后端url发送
Vue.prototype.url = {
	register: baseUrl + "/user/register",
	login: baseUrl + "/user/login",
	checkin: baseUrl + "/checkin/checkin",
	createFaceModel: baseUrl + "/checkin/createFaceModel",
	validCanCheckIn: baseUrl + "/checkin/validCanCheckIn",
	searchTodayCheckin: baseUrl + "/checkin/searchTodayCheckin",
	searchUserSummary: baseUrl + "/user/searchUserSummary",
	searchMonthCheckin: baseUrl + "/checkin/searchMonthCheckin",
	refreshMessage: baseUrl + "/message/refreshMessage",
	searchMessageByPage: baseUrl + "/message/searchMessageByPage",
	searchMessageById: baseUrl + "/message/searchMessageById",
	updateUnreadMessage: baseUrl + "/message/updateUnreadMessage",
	deleteMessageRefById: baseUrl + "/message/deleteMessageRefById",
	searchMyMeetingListByPage: baseUrl + "/meeting/searchMyMeetingListByPage",
	searchUserGroupByDept: baseUrl + "/user/searchUserGroupByDept",
	searchMembers: baseUrl + "/user/searchMembers",
	insertMeeting: baseUrl + "/meeting/insertMeeting",
	searchMeetingById: baseUrl + "/meeting/searchMeetingById",
	updateMeetingInfo: baseUrl + "/meeting/updateMeetingInfo",
	deleteMeetingById: baseUrl + "/meeting/deleteMeetingById",
	searchUserTaskListByPage: workflow + "/workflow/searchUserTaskListByPage",
	approvalMeeting: workflow + "/workflow/approvalMeeting",
	selectUserPhotoAndName: baseUrl + "/user/selectUserPhotoAndName",
	genUserSig: baseUrl + "/user/genUserSig",
	searchRoomIdByUUID: baseUrl + "/meeting/searchRoomIdByUUID",
	searchUserMeetingInMonth: baseUrl + "/meeting/searchUserMeetingInMonth"
}
//如果令牌刷新了,还要在本地存储。如果移动端每次发出Ajax,都要做这么多的判断,
//我们的重复性劳动太多了。所以尽可能的把Ajax封装
//二、封装Ajax移动端通过ajax向服务端提交请求,然后接收到的响应分若干种情况:
//   1.如果用户没有登陆系统,就跳转到登陆页面
//	 2.如果  用户权限不够,         就显示提示信息
//   3.如果  后端异常,             就提示异常信息。
//   4.如果  后端验证令牌不正确,    就提示信息
//	 5.如果后端正常处理请求,还要判断响应中是否有 Token。
//————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//AJAX的封装   方法：
//移动端发出请求，填好url地址，在main.js中集中管理全局变量
//url填写请求的地址，method是post还是get，data是上传的数据，fun是匿名函数（状态码200还是401？）
//一、封装全局路径上节课我们创建好了后端的register方法，
//那么移动端发出请求，首先要填写好URL地址。为了在移动端项目上集中管理URL路径，
//我们可以在main.js文件中用全局变量的语法，定义全局的URL地址，这样更加便于维护
//————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//ajax中定义了4个参数用来传输后台返回的信息，（向Java后台存进变量信息之后，需要返回信息）
//resp.statusCode服务器返回的状态码用于判断用户是否登录成功
//resp.data，开发者服务器中是否存有用户的token
//		1.服务器中有用户的token就向客户端返回存储token
//		2.如果服务器中没有用户的token，（说明是新用户？）
//		3.登录成功向客户端同步存储token
//该ajax是统一的，ajax不但只用在登录或者注册方面
Vue.prototype.ajax = function(url, method, data, fun) {
	uni.request({
		"url": url,
		"method": method,
		"header": {
			token: uni.getStorageInfoSync("token")
		},
		"data": data,
		success: function(resp) {
			//开发者服务器返回的状态码
			//登录失败了，重新登录
			if (resp.statusCode = 401) {
				uni.uni.redirectTo({
					//跳转到login登录页面
					url: '/pages/login/login.vue',
				}) //开发者网络返回代码401错误，登录失败，用户没有登录
			}
			//http状态码200，业务状态码200（R的状态码）
			else if (resp.statusCode == 200 && resp.data.code == 200) {
				let data = resp.data
				if (data.hasOwnProperty("token")) {
					let token = data.token
					//登陆成功了，向目录中存取token
					uni.setStorageSync("token", token)
					//执行匿名函数resp方法
					fun(resp)
				} else {
					uni.showToast({
						icon: "none",
						title: resp.data
					})
				}
			}
		}
	})
}
